package com.infiniteplugins.infinitedispensers;


import com.infiniteplugins.infinitecore.InfinitePlugin;
import com.infiniteplugins.infinitecore.command.CommandManager;
import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitecore.other.Metrics;
import com.infiniteplugins.infinitedispensers.commands.CommandGive;
import com.infiniteplugins.infinitedispensers.commands.commands.CommandInfiniteDispensers;
import com.infiniteplugins.infinitedispensers.commands.CommandReload;
import com.infiniteplugins.infinitedispensers.settings.Settings;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import com.google.common.base.Preconditions;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.api.dispenser.DispenserManager;
import com.infiniteplugins.infinitedispensers.api.dispenser.LevelManager;
import com.infiniteplugins.infinitedispensers.api.regions.ProtectionPluginHook;
import com.infiniteplugins.infinitedispensers.handlers.DispenseHandler;
//import com.infiniteplugins.infinitedispensers.hooks.*;
import com.infiniteplugins.infinitedispensers.managers.IDispenserManager;
import com.infiniteplugins.infinitedispensers.managers.ILevelManager;
import com.infiniteplugins.infinitedispensers.utils.Formatting;
import com.infiniteplugins.infinitedispensers.api.InfiniteDispensersAPI;
import com.infiniteplugins.infinitedispensers.listeners.BlockListeners;
import com.infiniteplugins.infinitedispensers.listeners.EntityListeners;
import com.infiniteplugins.infinitedispensers.listeners.InteractListeners;
import com.infiniteplugins.infinitedispensers.listeners.InventoryListeners;
import org.bukkit.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;


import java.io.File;
import java.io.IOException;
import java.util.*;

public class InfiniteDispensers extends InfinitePlugin implements InfiniteDispensersAPI {

    private static Map<String, FileConfiguration> configurations = new HashMap<>();
    private Set<ProtectionPluginHook> hooks = new HashSet<>();
    public Map<UUID, Dispenser> inShow = new HashMap<>();
    public Map<UUID, Block> entities = new HashMap<>();
    private DispenserManager dispenserManager;
    private DispenseHandler dispenseHandler;
    private static InfiniteDispensers instance;
    private CommandManager commandManager;
    private LevelManager levelManager;
    private Config menuConfig = new Config(this, "menus.yml");
    private boolean reload = false;

    @Override
    public void onConfigReload() {
        reload = true;
        Bukkit.getScheduler().cancelTasks(this);
        menuConfig.load();
        this.setLocale(getConfig().getString("system.language"), true);
        this.locale.reloadMessages();
        dispenserManager.save();
        reload = false;
    }

    @Override
    public List<Config> getExtraConfig() {
        return Collections.singletonList(menuConfig);
    }

    @Override
    public void onPluginEnable() {
        Bukkit.getScheduler().runTaskLater(this, () -> {
            instance = this;

            // Setup Config
            Settings.setupConfig();
            this.setLocale(Settings.LANGUGE_MODE.getString(), false);

            // Register commands
            this.commandManager = new CommandManager(this);
            this.commandManager.addCommand(new CommandInfiniteDispensers(this))
                    .addSubCommands(
                            new CommandGive(this),
                            new CommandReload(this));

            if (!new File(this.getDataFolder(), "menus.yml").exists()) {
                saveResource("menus.yml", false);
            }

            menuConfig.load();

            Arrays.asList("upgrading", "data")
                    .forEach(name -> configurations.put(name, load(name)));


            // Manager registration.
            levelManager = new ILevelManager(this);
            dispenserManager = new IDispenserManager(this, levelManager);
            dispenseHandler = new DispenseHandler(this);
            commandManager = new CommandManager(this);

            // Event registration.
            PluginManager manager = Bukkit.getPluginManager();
            manager.registerEvents(new InventoryListeners(this), this);
            manager.registerEvents(new InteractListeners(this), this);
            manager.registerEvents(new EntityListeners(this), this);
            manager.registerEvents(new BlockListeners(this), this);

            // Register default hooks
//            if (manager.isPluginEnabled("GriefPrevention")) registerProtectionHook(new HookGriefPrevention());
//            if (manager.isPluginEnabled("ASkyblock")) registerProtectionHook(new HookASkyBlock());
//            if (manager.isPluginEnabled("WorldGuard")) registerProtectionHook(new HookWorldGuard(this));
//            if (manager.isPluginEnabled("FactionsFramework")) registerProtectionHook(new HookFactions());
//            if (manager.isPluginEnabled("RedProtect")) registerProtectionHook(new HookRedProtect());

            // Start the Metrics
            new Metrics(this, 6362);

            //Dispenser saving
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> dispenserManager.save(), 600, 600);

            console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
            console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bInfinite Dispensers &ev" + instance.getDescription().getVersion()));
            console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Infinite Dispensers has been &bEnabled"));
            console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));


        },20);

    }


    @Override
    public void onPluginDisable() {
        if (dispenserManager != null)
            dispenserManager.save();
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bInfinite Dispensers &ev" + instance.getDescription().getVersion()));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bDisabling &7Infinite Dispensers..."));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));

    }

    private FileConfiguration load(String name) {
        File file = new File(getDataFolder(), name + ".yml");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            saveResource(file.getName(), false);
        }
        FileConfiguration configuration = new YamlConfiguration();
        try {
            configuration.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            consoleMessage("&cThe configuration was invalid or the server is 1.8. This version is not supported due to the Yaml Spigot packed back then. 1.8 is 4+ years old now.");
            instance.setEnabled(false);
            return null;
        }
        return configuration;
    }

    private static void consoleMessage(String string) {
        String message = Formatting.color(string);
        if (instance.getConfig().getBoolean("remove-console-color", false))
            message = ChatColor.stripColor(message);
        Bukkit.getLogger().info("" + message);
    }

    public void save(String configuration) {
        try {
            File configurationFile = new File(instance.getDataFolder(), configuration + ".yml");
            instance.getConfiguration(configuration).save(configurationFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int getItemLevel(ItemStack item) {
        if (item.getItemMeta().getDisplayName().contains(":")) {
            String arr[] = (item.getItemMeta().getDisplayName().replace("Â§", "").replace("§", "")).split(":");
            return Integer.parseInt(arr[0]);
        } else {
            return 0;
        }
    }

    @Override
    public void registerProtectionHook(ProtectionPluginHook hook) {
        Preconditions.checkNotNull(hook, "Cannot register null hook");
        Preconditions.checkNotNull(hook.getPlugin(), "Protection plugin hook returns null plugin instance (#getPlugin())");

        JavaPlugin hookPlugin = hook.getPlugin();
        for (ProtectionPluginHook existingHook : hooks) {
            if (existingHook.getPlugin().equals(hookPlugin)) {
                if (reload)
                    continue;
                throw new IllegalArgumentException("Hook already registered");
            }
        }
        FileConfiguration configuration = getConfiguration("hooks");
        String node = "hooks." + hookPlugin.getName();
        if (configuration.isSet(node)) {
            if (!configuration.getBoolean(node, false))
                return;
        } else {
            configuration.set(node, true);
            save("hooks");
        }
        hooks.add(hook);
        consoleMessage("&6Registered protection hook for plugin: " + hook.getPlugin().getName());
    }

    public boolean canBuild(Player player, Location location) {
        if (player.hasPermission(getDescription().getName() + ".bypass")) {
            return true;
        }

        for (ProtectionPluginHook hook : hooks)
            if (!hook.canBuild(player, location)) return false;
        return true;
    }

    public FileConfiguration getConfiguration(String name) {
        return configurations.get(name);
    }

    @Override
    public DispenserManager getDispenserManager() {
        return dispenserManager;
    }

    public DispenseHandler getDispenseHandler() {
        return dispenseHandler;
    }

    public static InfiniteDispensers getInstance() {
        return instance;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    @Override
    public LevelManager getLevelManager() {
        return levelManager;
    }

    public Config getMenuConfig() {
        return menuConfig;
    }

}

