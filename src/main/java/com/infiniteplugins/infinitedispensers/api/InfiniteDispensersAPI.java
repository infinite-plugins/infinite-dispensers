package com.infiniteplugins.infinitedispensers.api;



import com.infiniteplugins.infinitedispensers.api.dispenser.DispenserManager;
import com.infiniteplugins.infinitedispensers.api.dispenser.LevelManager;
import com.infiniteplugins.infinitedispensers.api.regions.ProtectionPluginHook;

/**
 * The access point of the InfiniteDispensersAPI, a class acting as a bridge between API
 * and plugin implementation. It is from here where developers should access the
 * important and core methods in the API. All static methods in this class will
 * call directly upon the implementation at hand (in most cases this will be the
 * EpicDispensers plugin itself), therefore a call to @link #getImplementation() is
 * not required and redundant in most situations. Method calls from this class are
 * preferred the majority of time, though an instance of @link InfiniteDispensers may
 * be passed if absolutely necessary.
 *
 * see InfiniteDispensers
 * @since 3.0.0
 */
public interface InfiniteDispensersAPI {

    /**
     * Register a protection plugin hook for EpicDispensers.
     *
     * @param hook The ProtectionPluginHook to be registered.
     */
    void registerProtectionHook(ProtectionPluginHook hook);

    /**
     * Grab the DispenserManager to handle Dispensers.
     *
     * @return The main DispenserManager
     */
    DispenserManager getDispenserManager();

    /**
     * Grabs the leveling manager of EpicDispensers
     *
     * @return The LevelManager class.
     */
    LevelManager getLevelManager();

}

