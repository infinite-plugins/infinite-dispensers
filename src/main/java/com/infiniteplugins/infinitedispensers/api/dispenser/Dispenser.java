package com.infiniteplugins.infinitedispensers.api.dispenser;

import org.bukkit.Location;

public interface Dispenser {

    /**
     * Grabs the location of the Dispenser.
     *
     * @return The Location of the Dispenser.
     */
    Location getLocation();

    /**
     * Grab the level instance of the Dispenser
     *
     * @return Level of the Dispenser.
     */
    Level getLevel();
}
