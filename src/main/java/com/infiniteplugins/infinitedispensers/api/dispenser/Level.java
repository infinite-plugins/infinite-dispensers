package com.infiniteplugins.infinitedispensers.api.dispenser;

import java.util.List;

public interface Level {

    /**
     * Grabs the description of the Level.
     *
     * @return The List<String> that makes up the description.
     */
    List<String> getDescription();

    /**
     * Checks if this level can be upgraded with Economy.
     *
     * @return boolean if this level can be upgraded with Economy.
     */
    boolean canUpgradeWithEconomy();

    /**
     * Checks if this level can be upgraded with XP.
     *
     * @return boolean if this level can be upgraded with Experience.
     */
    boolean canUpgradeWithXP();

    /**
     * Checks if this level can have the auto collection option.
     *
     * @return boolean if this level can have the auto collection option.
     */
    boolean hasAutoCollect();

    /**
     * Grab the cost it takes to upgrade to this level for xp.
     *
     * @return The int value it cost to upgrade from xp.
     */
    int getExperianceCost();

    /**
     * Grab the cost it takes to upgrade to this level for money.
     *
     * @return The int value it cost to upgrade from economy.
     */
    int getEconomyCost();

    /**
     * Check if the level has the auto-kill option.
     *
     * @return boolean if the level has the auto-kill option.
     */
    boolean isAutoKill();

    /**
     * Grabs the level int of the level.
     *
     * @return int level.
     */
    int getLevel();

    /**
     * Grabs the range or radius the level has.
     *
     * @return The dispenser range the level has.
     */
    int getRange();

    /**
     * Check if the level has the auto-kill option.
     *
     * @return boolean if the level has the auto-kill option.
     */
    boolean hasAutoShear();

}
