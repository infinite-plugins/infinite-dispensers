package com.infiniteplugins.infinitedispensers.api.dispenser;

import org.bukkit.block.Block;

import java.util.Map;
import java.util.Optional;

public interface DispenserManager {

    /**
     * Add a dispenser for EpicDispensers to keep track of.
     *
     * @param block The block the Dispenser is allocated to.
     * @param dispenser The Dispenser object to be added.
     */
    void addDispenser(Block block, Dispenser dispenser);

    /**
     * Grabs the Dispenser at block.
     *
     * @param block The block as to where the Dispenser is assumed to be.
     * @return The Dispenser found at the location.
     */
    Optional<Dispenser> getDispenser(Block block);

    /**
     * Removes a Dispenser from the storage.
     *
     * @param block The block the Dispenser is at.
     * @return The removed Dispenser.
     */
    Dispenser removeDispenser(Block block);

    /**
     * Grab an unmodifiable Map of the Dispensers that have been registered.
     *
     * @return unmodifiable Map of the Dispensers and Blocks.
     */
    Map<Block, Dispenser> getDispensers();

    /**
     * Saves the Dispensers to file.
     */
    void save();

}
