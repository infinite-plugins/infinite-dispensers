package com.infiniteplugins.infinitedispensers.api.dispenser;

import java.util.Map;

public interface LevelManager {

    /**
     * Add a level to the manager.
     *
     * @param level The level number, overriding old ones may cause issues.
     * @param costExperiance Cost of Experience
     * @param costEconomy Cost of Money
     * @param range The range of the Dispenser
     * @param autoKill If it should auto kill entities
     * @param upgradeXP If it allows to upgrade using xp
     * @param upgradeEco If it allows to upgrade using money
     * @param autoCollect If the Dispenser should pickup the items it breaks/kills.
     */
    void addLevel(int level, int costExperiance, int costEconomy, int range, boolean autoKill, boolean upgradeXP, boolean upgradeEco, boolean autoCollect, boolean autoShear);

    /**
     * Grab a level from an int.
     *
     * @param level The int to find.
     * @return The Level if found.
     */
    Level getLevel(int level);

    /**
     * Grabs the lowest level.
     *
     * @return The lowest Level
     */
    Level getLowestLevel();

    /**
     * Grabs the Highest level.
     *
     * @return The Highest Level.
     */
    Level getHighestLevel();

    /**
     * Grab all the levels and their position.
     *
     * @return The Levels and their Position.
     */
    Map<Integer, Level> getLevels();

}
