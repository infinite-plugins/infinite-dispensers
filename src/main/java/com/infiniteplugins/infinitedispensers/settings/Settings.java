package com.infiniteplugins.infinitedispensers.settings;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitecore.config.ConfigSetting;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import org.bukkit.event.Listener;

public class Settings implements Listener {

    static final Config config = InfiniteDispensers.getInstance().getCoreConfig();

    public static final ConfigSetting LANGUGE_MODE = new ConfigSetting(config, "system.language", "en_US",
                                                                       "The enabled language file.",
                                                                       "More language files (if available) can be found in the plugins data folder.");
    public static final ConfigSetting WRONG_ENUM = new ConfigSetting(config, "system.disable-wrong-enum-warning", false,
                                                                     "If on older versions than 1.13, you will probably be getting errors about wrong material enums.",
                                                                     "You can lookup proper enum names for your server to fix, or just set this to true and let InfiniteDispensers decide.",
                                                                     "We really recommend you take the time to configure the Material options below.",
                                                                     "1.9-1.12 are the enums in the \"quotes\" here: https://pastebin.com/Fe65HZnN",
                                                                     "1.8 can be found here https://github.com/Attano/Spigot-1.8/blob/master/org/bukkit/Material.java");
    // Where the hell did this go?
    public static final ConfigSetting ECONOMY_FORMAT = new ConfigSetting(config, "system.economy-format", "#,###.00");

    public static final ConfigSetting BROKEN_MAKES_NORMAL = new ConfigSetting(config, "system.breaking-makes-normal", true,
                                                                         "If a normal dispenser is broken, should it be turned into an Infinite Dispenser?");
    public static final ConfigSetting DISABLE_GIVE_MESSAGE = new ConfigSetting(config, "system.disable-give-message", false,
                                                                              "This will disable the message stating who gave the user a Dispenser.");

    public static final ConfigSetting DISPENSER_TICK = new ConfigSetting(config, "dispensers.tick", 30,
                                                                               "How often the dispensers are updated.");
    public static final ConfigSetting DISPENSER_ANIMATIONS = new ConfigSetting(config, "dispensers.animations", true,
                                                                         "Should the dispensers have an animation?");
    public static final ConfigSetting BOW_PARTICLE = new ConfigSetting(config, "dispensers.bow-particle", "SWEEP_ATTACK");
    public static final ConfigSetting SWORD_PARTICLE = new ConfigSetting(config, "dispensers.sword-particle", "SWEEP_ATTACK");
    public static final ConfigSetting SHEAR_PARTICLE = new ConfigSetting(config, "dispensers.shear-particle", "SWEEP_ATTACK");
    public static final ConfigSetting DISPENSER_ITEM_DURABILITY = new ConfigSetting(config, "dispensers.item-durability", true,
                                                                               "Should the dispensers do durability on items?");


    public static void setupConfig() {
        config.load();
        config.setAutoremove(true).setAutosave(true);
        config.saveChanges();
    }
}

