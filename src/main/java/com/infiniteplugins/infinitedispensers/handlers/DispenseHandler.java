package com.infiniteplugins.infinitedispensers.handlers;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.settings.Settings;
import com.infiniteplugins.infinitedispensers.utils.Utils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;

public class DispenseHandler {

	private InfiniteDispensers instance;
	private org.bukkit.entity.Sheep Sheep;

	public DispenseHandler(InfiniteDispensers instance) {
		this.instance = instance;
		Bukkit.getScheduler().scheduleSyncRepeatingTask(InfiniteDispensers.getInstance(), this::dispenserRunner, 0, Settings.DISPENSER_TICK.getInt());
	}

	@SuppressWarnings("deprecation")
	private void dispenserRunner() {
		Set<Dispenser> dispensers = Sets.newConcurrentHashSet(instance.getDispenserManager().getDispensers().values());

		for (Dispenser dispenser : dispensers) {
			Location location = dispenser.getLocation().clone();
			if (location == null || location.getBlock().getType() != Material.DISPENSER) {
				instance.getDispenserManager().removeDispenser(location.getBlock());
				continue;
			}

			int x = location.getBlockX() >> 4;
			int z = location.getBlockZ() >> 4;

			try {
				if (!location.getWorld().isChunkLoaded(x, z)) {
					continue;
				}
			} catch (Exception e) {
				continue;
			}
			Block b = location.getBlock();
			int range = dispenser.getLevel().getRange();


			org.bukkit.block.Dispenser bukkitDispenser = ((org.bukkit.block.Dispenser) b.getState());
			Inventory inventory = bukkitDispenser.getInventory();
//			Material woodenAxe = Utils.materialAttempt("WOOD_AXE", "WOODEN_AXE");
//			Material goldenAxe = Utils.materialAttempt("GOLD_AXE", "GOLDEN_AXE");
//			if (inventory.contains(woodenAxe)|| inventory.contains(Material.STONE_AXE)||
//					inventory.contains(Material.IRON_AXE)|| inventory.contains(goldenAxe)||
//					inventory.contains(Material.DIAMOND_AXE)) {
//				List<Block> nearbyblocks = Utils.getNearbyBlocks(bukkitDispenser.getLocation(), range, range, range);
//				boolean animate = false;
//				for (Block block : nearbyblocks) {
//					if (block.getType().name().contains("LOG")|| block.getType().name().contains("LEAVES")) {
//						animate = true;
//						if (dispenser.getLevel().hasAutoCollect()) {
//							for (ItemStack item : block.getDrops(new ItemStack(Material.DIAMOND_AXE))) {
//								if (block.getType().name().contains("LEAVES")) {
//									int random = new Random().nextInt(7) + 1;
//									if (random == 1)
//										inventory.addItem(item);
//								} else {
//									inventory.addItem(item);
//								}
//							}
//							block.setType(Material.AIR);
//						} else {
//							block.breakNaturally(new ItemStack(Material.DIAMOND_AXE));
//						}
//						for (ItemStack content : inventory.getContents()) {
//							if (content != null ) {
//								if (content.getType().name().endsWith("AXE") && configuration.getBoolean("dispensers.item-durability", true)) {
//									int random = new Random().nextInt(12) + 1;
//									if (random == 1) {
//										content.setDurability((short) (content.getDurability() + 1));
//										if (content.getDurability() > content.getType().getMaxDurability()) {
//											inventory.remove(content);
//											continue;
//										}
//									}
//									break;
//								}
//							}
//						}
//					}
//				}
//				if (animate)
//					animate(b, Material.DIAMOND_AXE);
//			}

			List<Entity> nearbyEntities = Lists.newArrayList(location.getWorld().getNearbyEntities(location.clone().add(0.5, 0.5, 0.5), range, 4, range));
			if (nearbyEntities.size() < 1)
				continue;
			Collections.shuffle(nearbyEntities);
			for (Entity nearby : nearbyEntities) {
				if (!nearby.isValid()
						|| nearby.getType() == EntityType.GIANT
						|| nearby.getType() == EntityType.PLAYER
						|| !(nearby instanceof LivingEntity)) {
					continue;
				}
				LivingEntity entity = (LivingEntity)nearby;
				int extra = 1;
				Material goldenSword = Utils.materialAttempt("GOLD_SWORD", "GOLDEN_SWORD");
				Material woodenSword = Utils.materialAttempt("WOOD_SWORD", "WOODEN_SWORD");
				if ((bukkitDispenser.getInventory().contains(Material.STONE_SWORD) ||
						bukkitDispenser.getInventory().contains(Material.DIAMOND_SWORD) ||
						bukkitDispenser.getInventory().contains(goldenSword) ||
						bukkitDispenser.getInventory().contains(Material.IRON_SWORD) ||
						bukkitDispenser.getInventory().contains(woodenSword)) && dispenser.getLevel().isAutoKill()) {

					ItemStack item = null;
					if (bukkitDispenser.getInventory().contains(Material.STONE_SWORD))
						item = processDurability(b, Material.STONE_SWORD, 0);
					else if (bukkitDispenser.getInventory().contains(woodenSword))
						item = processDurability(b, woodenSword, 0);
					else if (bukkitDispenser.getInventory().contains(Material.DIAMOND_SWORD))
						item = processDurability(b, Material.DIAMOND_SWORD, 0);
					else if (bukkitDispenser.getInventory().contains(goldenSword))
						item = processDurability(b, goldenSword, 0);
					else if (bukkitDispenser.getInventory().contains(Material.IRON_SWORD))
						item = processDurability(b, Material.IRON_SWORD, 0);
					else if (bukkitDispenser.getInventory().contains(woodenSword))
						item = processDurability(b, woodenSword, 0);

					instance.entities.put(entity.getUniqueId(), b);

					double enchantDamage = 0;

					if (item.getEnchantmentLevel(Enchantment.DAMAGE_ALL) != 0) {
						enchantDamage = enchantDamage + (item.getEnchantmentLevel(Enchantment.DAMAGE_ALL) * 1.25);
					}

					if (item.getEnchantmentLevel(Enchantment.FIRE_ASPECT) != 0) {
						if (item.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 1) {
							entity.setFireTicks(20 * 4);
						} else if (item.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 2) {
							entity.setFireTicks(20 * 8);
						}
					}

					Material mat = woodenSword;

					if (bukkitDispenser.getInventory().contains(woodenSword)) {
						((LivingEntity) entity).damage(4 + enchantDamage);
						mat = woodenSword;
					} else if (bukkitDispenser.getInventory().contains(goldenSword)) {
						((LivingEntity) entity).damage(4 + enchantDamage);
						mat = goldenSword;
					} else if (bukkitDispenser.getInventory().contains(Material.STONE_SWORD)) {
						((LivingEntity) entity).damage(5 + enchantDamage);
						mat = Material.STONE_SWORD;
					} else if (bukkitDispenser.getInventory().contains(Material.IRON_SWORD)) {
						((LivingEntity) entity).damage(6 + enchantDamage);
						mat = Material.IRON_SWORD;
					} else if (bukkitDispenser.getInventory().contains(Material.DIAMOND_SWORD)) {
						((LivingEntity) entity).damage(7 + enchantDamage);
						mat = Material.DIAMOND_SWORD;
					}
					Utils.playParticles(Settings.SWORD_PARTICLE.getString(), 25, .5, .8, .5, location.add(.5, .5, .5), entity.getLocation());
					processDurability(b, Material.SHEARS, extra);
					animate(b, mat);
					break;
				} else if (bukkitDispenser.getInventory().contains(Material.SHEARS) && dispenser.getLevel().hasAutoShear()) {
					int num = (int) Math.round(1 + (Math.random() * 3));
					if (!(entity instanceof Sheep) || ((Sheep) entity).isSheared() || !((Sheep) entity).isAdult()) {
						continue;
					}

					((Sheep) entity).setSheared(true);

					Wool woolColor = new Wool(((Sheep) entity).getColor());

					ItemStack wool = woolColor.toItemStack(num * extra);
					boolean teleport = dispenser.getLevel().hasAutoCollect();
					if (!teleport || !canDispense(bukkitDispenser.getInventory(), wool, num)) {
						entity.getLocation().getWorld().dropItemNaturally(entity.getLocation(), wool);
					} else {
						bukkitDispenser.getInventory().addItem(wool);
					}
					processDurability(b, Material.SHEARS, extra);
					Utils.playParticles(Settings.SHEAR_PARTICLE.getString(), 25, .5, .5, .5, location.add(.5, .5, .5), entity.getLocation());
					animate(b, Material.SHEARS);
					break;

				} else if (inventory.contains(Material.BOW) && (hasInfinity(b) || takeArrow(b))) {

					Location dispenserLocation = location.clone().add(0.5, 2, 0.5);
					ItemStack item = processDurability(b, Material.BOW, 0);
					Location entityLocation = entity.getEyeLocation();
					instance.entities.put(entity.getUniqueId(), b);

					// Setup velocity.
					double random = ((new Random().nextInt(((25-1) * 10 + 1)) + 10) / 100.0) / 10.0; //25 is the most accurate number found during testing. Low numbers good when entity is bobbing in water.
					double dX = dispenserLocation.getX() - entityLocation.getX() - random;
					double dY = dispenserLocation.getY() - entityLocation.getY() - random; //Randomized so it's granted to hit.
					double dZ = dispenserLocation.getZ() - entityLocation.getZ() - random;
					double pitch = Math.atan2(Math.sqrt(dZ * dZ + dX * dX), dY) + Math.PI;
					double yaw = Math.atan2(dZ, dX);
					double X = Math.sin(pitch) * Math.cos(yaw);
					double Y = Math.sin(pitch) * Math.sin(yaw);
					double Z = Math.cos(pitch);
					Vector vector = new Vector(X, Z, Y);
					Vector v = vector.multiply(3.0D);

					//Ray-tracing precision aiming.
					if (Utils.classExists("org.bukkit.FluidCollisionMode")) {
						if (Utils.methodExists(World.class, "rayTrace", Location.class, Vector.class, double.class, FluidCollisionMode.class, boolean.class, double.class, Predicate.class)) {
							RayTraceResult result = dispenserLocation.getWorld().rayTrace(dispenserLocation, v, dispenser.getLevel().getRange() + 1, FluidCollisionMode.ALWAYS, true, 0, e -> e.getType().isAlive() && e.getType() != EntityType.PLAYER);
							if (result != null) {
								// If path will hit a block or miss an entity.
								if (result.getHitBlock() != null) {
									//TODO instead of re-adding the items, change the poor takeArrow boolean condition away and test as own condition.
									inventory.addItem(new ItemStack(Material.ARROW));
									for (ItemStack bow : inventory.getContents()) {
										if (bow != null) {
											if (bow.getType() == Material.BOW) {
												bow.setDurability((short) (bow.getDurability() - 1));
												break;
											}
										}
									}
									continue;
								}
							}
						}
					}
					Arrow arrow = dispenserLocation.getWorld().spawn(dispenserLocation, Arrow.class);
//					arrow.setPickupStatus(PickupStatus.ALLOWED);
					if (item.getEnchantmentLevel(Enchantment.ARROW_FIRE) != 0) {
						arrow.setFireTicks(20 * 4);
					}
					arrow.setVelocity(v);
					Utils.playParticles(Settings.BOW_PARTICLE.getString(), 25, .5, .5, .5, location.add(.5, .5, .5), entityLocation);
					animate(b, Material.BOW);
					break;
				}
			}
		}
	}


	@SuppressWarnings("deprecation")
	public ItemStack processDurability(Block b, Material mat, int extra) {
		org.bukkit.block.Dispenser bukkitDispenser = ((org.bukkit.block.Dispenser) b.getState());
		int num = 0;
		if (extra <= 0)
			extra = 1;
		while (num != 9) {
			if (bukkitDispenser.getInventory().getItem(num) != null &&
					bukkitDispenser.getInventory().getItem(num).getType() == mat) {
				ItemStack item = bukkitDispenser.getInventory().getItem(num);

				short dur = item.getDurability();
				if (mat.equals(Material.SHEARS))
					dur = (short) (dur + 2);
				else
					dur = (short) (dur + 1);
				if (Settings.DISPENSER_ITEM_DURABILITY.getBoolean())
					item.setDurability((short) (dur * extra));
				if (dur >= mat.getMaxDurability())
					bukkitDispenser.getInventory().setItem(num, new ItemStack(Material.AIR));
				return item;
			}
			num++;
		}
		return null;
	}

	public void animate(Block b, Material mat) {
		if (b.getRelative(0, 1, 0).getType() != Material.AIR && Settings.DISPENSER_ANIMATIONS.getBoolean())
			return;
		Item i = b.getWorld().dropItem(b.getLocation().add(0.5, 1, 0.5), new ItemStack(mat, 1));

		// Support for EpicHoppers suction.
		i.setMetadata("grabbed", new FixedMetadataValue(InfiniteDispensers.getInstance(), "true"));

		i.setMetadata("betterdrops_ignore", new FixedMetadataValue(InfiniteDispensers.getInstance(), true));
		i.setPickupDelay(3600);

		i.setVelocity(new Vector(0, .3, 0));

		Bukkit.getScheduler().scheduleSyncDelayedTask(InfiniteDispensers.getInstance(), i::remove, 10);
	}

	public boolean takeArrow(Block b) {
		org.bukkit.block.Dispenser bukkitDispenser = ((org.bukkit.block.Dispenser) b.getState());
		int num = 0;
		while (num != 9) {
			if (bukkitDispenser.getInventory().getItem(num) != null &&
					bukkitDispenser.getInventory().getItem(num).getType() == Material.ARROW) {
				ItemStack item = bukkitDispenser.getInventory().getItem(num);

				if (item.getAmount() != 1)
					item.setAmount(item.getAmount() - 1);
				else
					bukkitDispenser.getInventory().setItem(num, new ItemStack(Material.AIR));
				return true;
			}
			num++;
		}
		return false;
	}

	public boolean hasInfinity(Block b) {
		org.bukkit.block.Dispenser bukkitDispenser = ((org.bukkit.block.Dispenser) b.getState());
		for (int i = 0; i < 9; i += 2) {
			if (bukkitDispenser.getInventory().getItem(i) != null
					&& bukkitDispenser.getInventory().getItem(i).getType() == Material.BOW) {
				ItemStack item = bukkitDispenser.getInventory().getItem(i);

				if (item.getEnchantmentLevel(Enchantment.ARROW_INFINITE) != 0)
					return true;
			}
		}
		return false;
	}

	public boolean canDispense(Inventory i, ItemStack item, int hop) {
		if (i.firstEmpty() != -1) {
			return true;
		}
		boolean can = false;
		for (ItemStack it : i.getContents()) {
			if (it == null) {
				can = true;
				break;
			}
			if (it.isSimilar(item)
					&& (it.getAmount() + hop) <= it.getMaxStackSize()) {
				can = true;
				break;
			}
		}
		return can;
	}

}
