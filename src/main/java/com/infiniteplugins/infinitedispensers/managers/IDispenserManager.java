package com.infiniteplugins.infinitedispensers.managers;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.api.dispenser.DispenserManager;
import com.infiniteplugins.infinitedispensers.api.dispenser.LevelManager;
import com.infiniteplugins.infinitedispensers.objects.IDispenser;
import com.infiniteplugins.infinitedispensers.utils.Serializer;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class IDispenserManager implements DispenserManager {

	private final Map<Block, Dispenser> dispensers = new HashMap<>();
	private final InfiniteDispensers instance;
	private final FileConfiguration data;

	public IDispenserManager(InfiniteDispensers instance, LevelManager levelManager) {
		this.instance = instance;
		this.data = instance.getConfiguration("data");
		if (data.isConfigurationSection("data.dispensers")) {
			for (String locationStr : data.getConfigurationSection("data.dispensers").getKeys(false)) {
				Location location = Serializer.unserializeLocation(locationStr);
				if (location != null) {
					int level = data.getInt("data.dispensers." + locationStr + ".level");
					Dispenser dispenser = new IDispenser(location, levelManager.getLevel(level));
					addDispenser(location.getBlock(), dispenser);
				}
			}
		}
	}

	/*
	 * Saves registered dispensers to file.
	 */
	public void save() {
		if (!dispensers.isEmpty()) {
			// Delete old information, it's stored in map already.
			data.set("data.dispensers", null);

			// Dump DispenserManager to file.
			for (Dispenser dispenser : dispensers.values()) {
				String location = Serializer.serializeLocation(dispenser.getLocation());
				data.set("data.dispensers." + location + ".level", dispenser.getLevel().getLevel());
			}
			// Save to file
			instance.save("data");
		}
	}

	@Override
	public void addDispenser(Block block, Dispenser dispenser) {
		dispensers.put(block, dispenser);
	}

	@Override
	public Optional<Dispenser> getDispenser(Block block) {
		return dispensers.entrySet().parallelStream()
				.filter(entry -> block.getLocation().equals(entry.getKey().getLocation()))
				.map(Map.Entry::getValue)
				.findFirst();
	}

	@Override
	public Map<Block, Dispenser> getDispensers() {
		return Collections.unmodifiableMap(dispensers);
	}

	@Override
	public Dispenser removeDispenser(Block block) {
		return dispensers.remove(block);
	}

}
