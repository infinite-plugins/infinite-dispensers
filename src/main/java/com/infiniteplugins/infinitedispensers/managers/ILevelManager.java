package com.infiniteplugins.infinitedispensers.managers;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Level;
import com.infiniteplugins.infinitedispensers.api.dispenser.LevelManager;
import com.infiniteplugins.infinitedispensers.objects.ILevel;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Collections;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class ILevelManager implements LevelManager {
	
	private final NavigableMap<Integer, Level> registeredLevels = new TreeMap<>();

	public ILevelManager(InfiniteDispensers instance) {
		FileConfiguration upgrading = instance.getConfiguration("upgrading");
		if (upgrading.isConfigurationSection("upgrading.levels")) {
			for (String levelName : upgrading.getConfigurationSection("upgrading.levels").getKeys(false)) {
				int level = Integer.valueOf(levelName.split("-")[1]);
				boolean upgradeEconomy = upgrading.getBoolean("upgrading.levels." + levelName + ".allow-economy-upgrading", true);
				boolean upgradeXP = upgrading.getBoolean("upgrading.levels." + levelName + ".allow-xp-upgrading", true);
				boolean autoCollect = upgrading.getBoolean("upgrading.levels." + levelName + ".auto-collect", false);
				boolean autoKill = upgrading.getBoolean("upgrading.levels." + levelName + ".auto-kill", true);
				boolean autoShear = upgrading.getBoolean("upgrading.levels." +levelName + ".auto-shear", false);
				int costExperiance = upgrading.getInt("upgrading.levels." + levelName + ".cost-xp", 30);
				int costEconomy = upgrading.getInt("upgrading.levels." + levelName + ".cost-money", 1000);
				int radius = upgrading.getInt("upgrading.levels." + levelName + ".radius", 5);
				addLevel(level, costExperiance, costEconomy, radius, autoKill, upgradeXP, upgradeEconomy, autoCollect, autoShear);
			}
		}
	}

	@Override
	public void addLevel(int level, int costExperiance, int costEconomy, int range, boolean autoKill, boolean upgradeXP, boolean upgradeEconomy, boolean autoCollect, boolean autoShear) {
		registeredLevels.put(level, new ILevel(level, costExperiance, costEconomy, range, autoKill, upgradeXP, upgradeEconomy, autoCollect, autoShear));
	}
	
	@Override
	public Map<Integer, Level> getLevels() {
		return Collections.unmodifiableMap(registeredLevels);
	}

	@Override
	public Level getLevel(int level) {
		return registeredLevels.get(level);
	}

	@Override
	public Level getLowestLevel() {
		return registeredLevels.firstEntry().getValue();
	}

	@Override
	public Level getHighestLevel() {
		return registeredLevels.lastEntry().getValue();
	}

}
