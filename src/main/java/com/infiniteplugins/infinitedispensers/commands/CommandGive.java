package com.infiniteplugins.infinitedispensers.command.commands;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.utils.Formatting;
import com.infiniteplugins.infinitedispensers.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandGive extends AbstractCommand {

	public CommandGive(AbstractCommand parent) {
		super("give", parent, true);
	}

	@Override
	protected ReturnType runCommand(InfiniteDispensers instance, CommandSender sender, String... args) {
		if (args.length < 4)
			return ReturnType.SYNTAX_ERROR;
		Player player = Bukkit.getPlayer(args[1]);
		int level;
		int amount;
		try {
			level = Integer.parseInt(args[2]);
			amount = Integer.parseInt(args[3]);
		} catch (Exception e) {
			new MessageBuilder("messages.invalid-give")
					.setPlaceholderObject(sender)
					.replace("%level%", args[2])
					.replace("%amount%", args[3])
					.send(sender);
			return ReturnType.FAILURE;
		}
		if (!InfiniteDispensers.getInstance().getConfiguration("upgrading").isConfigurationSection("upgrading.levels.level-" + level)) {
			new MessageBuilder("messages.level-doesnt-exist")
					.setPlaceholderObject(sender)
					.replace("%amount%", amount)
					.replace("%level%", level)
					.send(sender);
			return ReturnType.FAILURE;
		}
		if (player != null && amount > 0 && level > 0) {
			ItemStack item = new ItemStack(Material.DISPENSER, amount);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(Formatting.color(Utils.formatName(level, true)));
			item.setItemMeta(meta);
			player.getInventory().addItem(item);
			new MessageBuilder("messages.givenTo")
					.setPlaceholderObject(player)
					.replace("%amount%", amount)
					.replace("%level%", level)
					.send(sender);
			if (player != sender && !instance.getConfig().getBoolean("disable-give-message", false))
				new MessageBuilder("messages.givenFrom")
						.setPlaceholderObject(sender)
						.replace("%amount%", amount)
						.replace("%level%", level)
						.send(player);
			return ReturnType.SUCCESS;
		}
		if (player == null)
			new MessageBuilder("messages.no-player")
					.replace("%name%", args[1])
					.send(sender);
		if (level <= 0)
			new MessageBuilder("messages.cant-be-zero").send(sender);
		if (amount <= 0)
			new MessageBuilder("messages.cant-have-none").send(sender);
		return ReturnType.FAILURE;
	}

	@Override
	public String getPermissionNode() {
		return InfiniteDispensers.getInstance().getConfig().getString("permissions.give", "infinite.dispensers.give");
	}
	
	@Override
	public String getDescription() {
		return "Gives Dispensers to players with the defined level and amount.";
	}

	@Override
	public String getSyntax() {
		return "/id give <player> <level> <amount>";
	}

}
