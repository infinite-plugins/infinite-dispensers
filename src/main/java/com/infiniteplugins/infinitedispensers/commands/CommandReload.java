package com.infiniteplugins.infinitedispensers.command.commands;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import org.bukkit.command.CommandSender;

public class CommandReload extends AbstractCommand {

	public CommandReload(AbstractCommand parent) {
		super("reload", parent, true);
	}

	@Override
	protected ReturnType runCommand(InfiniteDispensers instance, CommandSender sender, String... args) {
		instance.reload();
		new MessageBuilder("messages.reloaded")
				.setPlaceholderObject(sender)
				.send(sender);
		return ReturnType.SUCCESS;
	}

	@Override
	public String getPermissionNode() {
		return InfiniteDispensers.getInstance().getConfig().getString("permissions.admin", "infinite.dispensers.admin");
	}
	
	@Override
	public String getDescription() {
		return "Reload the Configuration and Language files.";
	}

	@Override
	public String getSyntax() {
		return "/id reload";
	}

}
