package com.infiniteplugins.infinitedispensers.command.commands;

import com.infiniteplugins.infinitecore.command.AbstractCommand;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.List;

public class CommandInfiniteDispensers extends AbstractCommand {

	final InfiniteDispensers instance;

	public CommandInfiniteDispensers(InfiniteDispensers instance) {
		super(false, "InfiniteDispensers");
		this.instance = instance;
	}

	@Override
	protected ReturnType runCommand(CommandSender sender, String... args) {
		sender.sendMessage("");
		instance.getLocale().newMessage("&7Version &b" + instance.getDescription().getVersion()).sendPrefixedMessage(sender);

		for (AbstractCommand command : instance.getCommandManager().getAllCommands()) {
			if (command.getPermissionNode() == null || sender.hasPermission(command.getPermissionNode())) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8 - &b" + command.getSyntax() + "&8 - &7" + command.getDescription()));
			}
		}
		sender.sendMessage("");

		return ReturnType.SUCCESS;
	}

	@Override
	protected List<String> onTab(CommandSender sender, String... args) {
		return null;
	}

	@Override
	public String getPermissionNode() {
		return null;
	}

	@Override
	public String getSyntax() {
		return "/id";
	}

	@Override
	public String getDescription() {
		return "Displays this page.";
	}
}

