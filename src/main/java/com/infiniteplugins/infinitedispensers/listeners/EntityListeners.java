package com.infiniteplugins.infinitedispensers.listeners;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.DispenserManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.stream.Collectors;

public class EntityListeners implements Listener {

	private final InfiniteDispensers instance;
	private final DispenserManager manager;

	public EntityListeners(InfiniteDispensers instance) {
		this.manager = instance.getDispenserManager();
		this.instance = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityExplode(EntityExplodeEvent event) {
		Set<com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser> dispensers = event.blockList().parallelStream()
				.map(block -> instance.getDispenserManager().getDispenser(block))
				.filter(optional -> optional.isPresent())
				.map(optional -> optional.get())
				.collect(Collectors.toSet());
		// Sync back to Bukkit
		dispensers.forEach(dispenser -> BlockListeners.drop(dispenser));
	}

	@EventHandler
	public void onDrop(EntityDeathEvent event) {
		UUID uuid = event.getEntity().getUniqueId();
		if (!instance.entities.containsKey(uuid))
			return;
		Block block = instance.entities.get(uuid);
		instance.entities.remove(uuid);
		if (block == null || block.getType() != Material.DISPENSER) {
			return;
		}
		Optional<com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser> optional = manager.getDispenser(block);
		if (optional.isPresent()) {
			com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser dispenser = optional.get();
			if (dispenser.getLevel().hasAutoCollect()) {
				List<ItemStack> list = new ArrayList<>();
				for (ItemStack item : event.getDrops()) {
					if (instance.getDispenseHandler().canDispense(((Dispenser) block.getState()).getInventory(), item, item.getAmount())) {
						((Dispenser) block.getState()).getInventory().addItem(item);
						list.add(item);
					}
				}
				for (ItemStack item : list) {
					event.getDrops().remove(item);
				}
			}
		}
	}

}
