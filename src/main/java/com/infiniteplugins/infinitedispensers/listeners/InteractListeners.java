package com.infiniteplugins.infinitedispensers.listeners;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.objects.IDispenser;
import com.infiniteplugins.infinitedispensers.settings.Settings;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Optional;

/**
 * Created by songoda on 3/14/2017.
 */
public class InteractListeners implements Listener {

	private InfiniteDispensers instance;

	public InteractListeners(InfiniteDispensers instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onBlockInteract(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		Player player = event.getPlayer();
		if (block == null
				|| block.getType() != Material.DISPENSER
				|| event.getAction() != Action.LEFT_CLICK_BLOCK
				|| player.isSneaking()
				|| !instance.canBuild(player, block.getLocation())) {
			return;
		}
		Optional<Dispenser> optional = instance.getDispenserManager().getDispenser(block);
		if (!optional.isPresent()) {
			if (Settings.BROKEN_MAKES_NORMAL.getBoolean() == true) {
				return;
			}
			event.setCancelled(true);
			String message = instance.getLocale().getMessage("messages.transform").getPrefixedMessage();
			event.getPlayer().sendMessage(message);
			instance.getDispenserManager().addDispenser(block, new IDispenser(block, instance.getLevelManager().getLowestLevel()));
		} else {
			event.setCancelled(true);
			((IDispenser)optional.get()).overview(player);
		}
	}

}
