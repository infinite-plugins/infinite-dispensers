package com.infiniteplugins.infinitedispensers.listeners;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.api.dispenser.DispenserManager;
import com.infiniteplugins.infinitedispensers.objects.IDispenser;
import com.infiniteplugins.infinitedispensers.utils.Formatting;
import com.infiniteplugins.infinitedispensers.utils.Utils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


public class BlockListeners implements Listener {
	
	private final InfiniteDispensers instance;
	private final DispenserManager manager;

	public BlockListeners(InfiniteDispensers instance) {
		this.manager = instance.getDispenserManager();
		this.instance = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockExplode(BlockExplodeEvent event) {
		Set<Dispenser> dispensers = event.blockList().parallelStream()
				.map(block -> instance.getDispenserManager().getDispenser(block))
				.filter(optional -> optional.isPresent())
				.map(optional -> optional.get())
				.collect(Collectors.toSet());
		// Sync back to Bukkit
		dispensers.forEach(dispenser -> drop(dispenser));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		ItemStack item = event.getItemInHand().clone();
		if (!item.getItemMeta().hasDisplayName())
			return;
		int level = instance.getItemLevel(item);
		if (event.getBlock().getType() == Material.DISPENSER && level != 0) {
			Dispenser dispenser = new IDispenser(event.getBlock(), instance.getLevelManager().getLevel(level));
			manager.addDispenser(event.getBlock(), dispenser);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		if (block.getType() != Material.DISPENSER)
			return;
		Optional<Dispenser> optional = manager.getDispenser(block);
		if (optional.isPresent()) {
			drop(optional.get());
		}
		manager.removeDispenser(block);
	}
	
	public static void drop(Dispenser dispenser) {
		ItemStack item = new ItemStack(Material.DISPENSER, 1);
		ItemMeta itemmeta = item.getItemMeta();
		itemmeta.setDisplayName(Formatting.color(Utils.formatName(dispenser.getLevel().getLevel(), true)));
		item.setItemMeta(itemmeta);
		Block block = dispenser.getLocation().getBlock();
		block.setType(Material.AIR);
		Location location = block.getLocation();
		location.getWorld().dropItemNaturally(block.getLocation(), item);
	}

}
