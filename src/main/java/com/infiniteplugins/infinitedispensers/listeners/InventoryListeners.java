package com.infiniteplugins.infinitedispensers.listeners;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.objects.IDispenser;
import com.infiniteplugins.infinitedispensers.utils.Utils;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryListeners implements Listener {

	private final InfiniteDispensers instance;

	public InventoryListeners(InfiniteDispensers instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory inventory = event.getInventory();
		ItemStack item = event.getCurrentItem();
		if (inventory == null || item == null)
			return;
		int slot = event.getSlot();
		if (instance.inShow.containsKey(player.getUniqueId())) {
			event.setCancelled(true);
			IDispenser dispenser = (IDispenser) instance.inShow.get(player.getUniqueId());
			if (slot == 11 || slot == 15) {
				if (dispenser.getLevel().equals(instance.getLevelManager().getHighestLevel())) {
					Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.cant-upgrade").playTo(player);
					return;
				}
				String xpPermission = "infinite.dispensers.upgrade.xp";
				String economyPermission = "infinite.dispensers.upgrade.economy";
				if (slot == 11 && player.hasPermission(xpPermission)) {
					if (!item.getItemMeta().getDisplayName().equals("§l")) {
						dispenser.upgrade("XP", player);
						player.closeInventory();
					}
				} else if (event.getSlot() == 15 && player.hasPermission(economyPermission)) {
					if (!event.getCurrentItem().getItemMeta().getDisplayName().equals("§l")) {
						dispenser.upgrade("ECO", player);
						player.closeInventory();
					}
				}
			}
		}
		if (slot != 64537 && event.getAction() != InventoryAction.NOTHING
				&& inventory.getType() == InventoryType.ANVIL
				&& item.getType() == Material.DISPENSER) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		instance.inShow.remove(player.getUniqueId());
	}

}
