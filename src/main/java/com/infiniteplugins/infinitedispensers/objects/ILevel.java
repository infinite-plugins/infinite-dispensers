package com.infiniteplugins.infinitedispensers.objects;


import com.google.common.collect.Lists;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Level;

import java.util.ArrayList;
import java.util.List;

public class ILevel implements Level {

	private final boolean autokill, upgradeXP, upgradeEco, autoCollect, autoShear;
	private final int level, costExperiance, costEconomy, range;
	private List<String> description = new ArrayList<>();

	public ILevel(int level, int costExperiance, int costEconomy, int range, boolean autokill, boolean upgradeXP, boolean upgradeEco, boolean autoCollect, boolean autoShear) {
		this.costExperiance = costExperiance;
		this.costEconomy = costEconomy;
		this.autoCollect = autoCollect;
		this.upgradeEco = upgradeEco;
		this.upgradeXP = upgradeXP;
		this.autokill = autokill;
		this.autoShear = autoShear;
		this.level = level;
		this.range = range;
		description.add(InfiniteDispensers.getInstance().getLocale().getMessage("menus.range")
								.processPlaceholder("range", range + "")
								.getMessage());
		if (autoCollect)
			description.add(InfiniteDispensers.getInstance().getLocale().getMessage("menus.auto-collect")
				.processPlaceholder("enabled", autoCollect)
				.getMessage());
		if (autokill)
			description.add(InfiniteDispensers.getInstance().getLocale().getMessage("menus.auto-kill")
					.processPlaceholder("enabled", autokill)
					.getMessage());
		if (autoShear)
			description.add(InfiniteDispensers.getInstance().getLocale().getMessage("menus.auto-shear")
									.processPlaceholder("enabled", autoShear)
									.getMessage());
	}
	
	@Override
	public boolean canUpgradeWithEconomy() {
		return upgradeEco;
	}

	@Override
	public List<String> getDescription() {
		return Lists.newArrayList(description);
	}
	
	@Override
	public boolean canUpgradeWithXP() {
		return upgradeXP;
	}
	
	@Override
	public boolean hasAutoCollect() {
		return autoCollect;
	}
	
	@Override
	public int getExperianceCost() {
		return costExperiance;
	}

	@Override
	public boolean isAutoKill() {
		return autokill;
	}

	@Override
	public boolean hasAutoShear() {
		return autoShear;
	}

	@Override
	public int getEconomyCost() {
		return costEconomy;
	}
	
	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public int getRange() {
		return range;
	}

}
