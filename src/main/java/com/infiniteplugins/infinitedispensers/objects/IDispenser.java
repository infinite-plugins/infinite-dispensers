package com.infiniteplugins.infinitedispensers.objects;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.api.dispenser.Dispenser;
import com.infiniteplugins.infinitedispensers.api.dispenser.Level;
import com.infiniteplugins.infinitedispensers.utils.Formatting;
import com.infiniteplugins.infinitedispensers.utils.Utils;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.ArrayList;
import java.util.List;

public class IDispenser implements Dispenser {

	private Location location;
	private Level level;

	public IDispenser(Location location, Level level) {
		this.location = location;
		this.level = level;
		syncName();
	}

	public IDispenser(Block block, Level level) {
		this(block.getLocation(), level);
	}

	//TODO This is bloated, make a menu system.
	public void overview(Player p) {
		InfiniteDispensers instance = InfiniteDispensers.getInstance();
		if (!p.hasPermission("infinite.dispensers.overview"))
			return;
		Inventory i = Bukkit.createInventory(null, 27, Formatting.color(Utils.formatName(level.getLevel(), false)));
		Level nextLevel = instance.getLevelManager().getHighestLevel().getLevel() > level.getLevel() ? instance.getLevelManager().getLevel(level.getLevel() + 1) : null;
		
		// Setup the background
		int nu = 0;
		while (nu != 27) {
			i.setItem(nu, Utils.getGlass());
			nu++;
		}
		i.setItem(0, Utils.getBackgroundGlass(true));
		i.setItem(1, Utils.getBackgroundGlass(true));
		i.setItem(2, Utils.getBackgroundGlass(false));
		i.setItem(6, Utils.getBackgroundGlass(false));
		i.setItem(7, Utils.getBackgroundGlass(true));
		i.setItem(8, Utils.getBackgroundGlass(true));
		i.setItem(9, Utils.getBackgroundGlass(true));
		i.setItem(10, Utils.getBackgroundGlass(false));
		i.setItem(16, Utils.getBackgroundGlass(false));
		i.setItem(17, Utils.getBackgroundGlass(true));
		i.setItem(18, Utils.getBackgroundGlass(true));
		i.setItem(19, Utils.getBackgroundGlass(true));
		i.setItem(20, Utils.getBackgroundGlass(false));
		i.setItem(24, Utils.getBackgroundGlass(false));
		i.setItem(25, Utils.getBackgroundGlass(true));
		i.setItem(26, Utils.getBackgroundGlass(true));
		
		// Current level
		ItemStack item = new ItemStack(Material.DISPENSER, 1);
		ItemMeta itemmeta = item.getItemMeta();
		itemmeta.setDisplayName(instance.getLocale().getMessage("menus.current-level")
					.processPlaceholder("level", level.getLevel() + "")
					.getMessage());
		List<String> lore = level.getDescription();
		lore.add("");
		if (nextLevel == null)
			lore.add(instance.getLocale().getMessage("menus.already-maxed")
					.processPlaceholder("level", level.getLevel() + "")
					.getMessage());
		else {
			lore.add(instance.getLocale().getMessage("menus.next-level")
					.processPlaceholder("level", level.getLevel() + "")
					.getMessage());
			lore.addAll(nextLevel.getDescription());
		}
		itemmeta.setLore(lore);
		item.setItemMeta(itemmeta);
		i.setItem(13, item);
		
		// XP Upgrade
		ItemStack itemXP = Utils.getItem("menus.upgrade-xp", "EXP_BOTTLE");
		ItemMeta itemmetaXP = itemXP.getItemMeta();
		itemmetaXP.setDisplayName(instance.getLocale().getMessage("menus.upgrade-with-xp").getMessage());
		itemXP.setItemMeta(itemmetaXP);
		List<String> loreXP = new ArrayList<>();
		if (nextLevel != null)
			loreXP.add(instance.getLocale().getMessage("menus.upgrade-with-xp-lore")
							   .processPlaceholder("cost", nextLevel.getEconomyCost())
							   .getMessage());
		else
			loreXP.add(instance.getLocale().getMessage("menus.already-maxed")
							   .processPlaceholder("level", level.getLevel())
							   .getMessage());
		itemmetaXP.setLore(loreXP);
		itemXP.setItemMeta(itemmetaXP);
		
		// Economy Upgrade
		ItemStack itemECO = Utils.getItem("menus.upgrade-economy", "DIAMOND");
		ItemMeta itemmetaECO = itemECO.getItemMeta();
		itemmetaECO.setDisplayName(instance.getLocale().getMessage("menus.upgrade-with-economy").getMessage());
		List<String> loreECO = new ArrayList<>();
		if (nextLevel != null)
			loreECO.add(instance.getLocale().getMessage("menus.upgrade-with-economy-lore")
								.processPlaceholder("cost", nextLevel.getEconomyCost())
								.getMessage());
		else
			loreECO.add(instance.getLocale().getMessage("menus.already-maxed")
								.processPlaceholder("level", level.getLevel())
								.getMessage());
		itemmetaECO.setLore(loreECO);
		itemECO.setItemMeta(itemmetaECO);

		String xpPermission = "infinite.dispensers.upgrade.xp";
		if (level.canUpgradeWithXP() && p.hasPermission(xpPermission)) {
			i.setItem(11, itemXP);
		} else {
			ItemStack cantItem = Utils.getItem("menus.cant-upgrade-xp");
			i.setItem(11, cantItem);
		}
		String ecoPermission = "infinite.dispensers.upgrade.economy";
		if (level.canUpgradeWithEconomy() && p.hasPermission(ecoPermission)) {
			i.setItem(15, itemECO);
		} else {
			ItemStack cantItem = Utils.getItem("menus.cant-upgrade-economy");
			i.setItem(15, cantItem);
		}

		p.openInventory(i);
		instance.inShow.put(p.getUniqueId(), this);
	}

	public void upgrade(String type, Player player) {
		InfiniteDispensers instance = InfiniteDispensers.getInstance();
		if (instance.getLevelManager().getLevels().containsKey(this.level.getLevel() + 1)) {

			Level level = instance.getLevelManager().getLevel(this.level.getLevel() + 1);
			int cost;
			if (type == "XP") {
				cost = level.getExperianceCost();
			} else {
				cost = level.getEconomyCost();
			}

			if (type == "ECO") {
				if (instance.getServer().getPluginManager().getPlugin("Vault") != null) {
					RegisteredServiceProvider<Economy> rsp = instance.getServer().getServicesManager().getRegistration(Economy.class);
					if (rsp == null) {
						String message = instance.getLocale().getMessage("messages.not-installed-properly")
								.processPlaceholder("cost", cost + "")
								.getPrefixedMessage();
						player.sendMessage(message);
						return;
					}
					Economy econ = rsp.getProvider();
					if (econ.has(player, cost)) {
						econ.withdrawPlayer(player, cost);
						upgradeFinal(level, player);
					} else {
						String message = instance.getLocale().getMessage("messages.cannot-afford")
								.processPlaceholder("cost", cost + "")
								.getPrefixedMessage();
						player.sendMessage(message);
					}
				} else {
					String message = instance.getLocale().getMessage("messages.vault-not-installed")
							.processPlaceholder("cost", cost + "")
							.getPrefixedMessage();
					player.sendMessage(message);
				}
			} else if (type == "XP") {
				if (player.getLevel() >= cost || player.getGameMode() == GameMode.CREATIVE) {
					if (player.getGameMode() != GameMode.CREATIVE) {
						player.setLevel(player.getLevel() - cost);
					}
					upgradeFinal(level, player);
				} else {
					String message = instance.getLocale().getMessage("messages.cannot-afford")
							.processPlaceholder("cost", cost + "")
							.getPrefixedMessage();
					player.sendMessage(message);
				}
			}
		}
	}

	private void upgradeFinal(Level level, Player player) {
		InfiniteDispensers instance = InfiniteDispensers.getInstance();
		FileConfiguration upgrading = instance.getConfiguration("upgrading");
		this.level = level;
		syncName();
		if (instance.getLevelManager().getHighestLevel() != level) {
			String message = instance.getLocale().getMessage("messages.success")
					.processPlaceholder("level", level.getLevel() + "")
					.getPrefixedMessage();
			player.sendMessage(message);
		} else {
			String message = instance.getLocale().getMessage("messages.maxed")
					.processPlaceholder("level", level.getLevel() + "")
					.getPrefixedMessage();
			player.sendMessage(message);
		}
		Utils.playParticles(upgrading.getString("upgrading.particle", "SPELL_WITCH"), 200, .5, .5, .5, location.clone().add(.5, .5, .5));
		if (upgrading.getBoolean("upgrading.sounds.enabled", true)) {
			if (instance.getLevelManager().getHighestLevel() == level) {
				Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.highest-level").playTo(player);
			} else {
				Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.upgrade-1").playTo(player);
				Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.upgrade-2").playTo(player);
				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, () -> Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.upgrading-1").playTo(player), 5L);
				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, () -> Utils.getSound(instance.getConfiguration("upgrading"), "upgrading.sounds.upgrading-2").playTo(player), 10L);
			}
		}
	}

	private void syncName() {
		Block block = location.getBlock();
		org.bukkit.block.Dispenser dispenser = null;
		if (block.getState() instanceof org.bukkit.block.Dispenser)
			dispenser = (org.bukkit.block.Dispenser) block.getState();
		if (block instanceof org.bukkit.block.Dispenser)
			dispenser = (org.bukkit.block.Dispenser) block;
		if (dispenser == null)
			return;
		if (!Utils.classExists("org.bukkit.Nameable"))
			return;
		if (!Utils.methodExists(Nameable.class, "setCustomName", String.class))
			return;
		dispenser.setCustomName(Utils.formatName(level.getLevel(), false));
		dispenser.update(true);
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public Level getLevel() {
		return level;
	}

}
