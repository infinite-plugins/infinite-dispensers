package com.infiniteplugins.infinitedispensers.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Arrays;
import java.util.List;

public class Serializer {

	public static String serializeLocation(Location location) {
		if (location == null)
			return "";
		String w = location.getWorld().getName();
		double x = location.getX();
		double y = location.getY();
		double z = location.getZ();
		String str = "w:" + w + "x:" + x + "y:" + y + "z:" + z;
		return str.replace(".0", "").replace(".", "~");
	}

	public static Location unserializeLocation(String input) {
		if (input == null || input.equals(""))
			return null;
		input = input.replace("y:", ":").replace("z:", ":").replace("w:", "").replace("x:", ":").replace("~", ".");
		List<String> arguments = Arrays.asList(input.split("\\s*:\\s*"));
		World world = Bukkit.getWorld(arguments.get(0));
		double x = Double.parseDouble(arguments.get(1)), y = Double.parseDouble(arguments.get(2)), z = Double.parseDouble(arguments.get(3));
		Location location = new Location(world, x, y, z, 0, 0);
		return location;
	}

}
