package com.infiniteplugins.infinitedispensers.utils;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitecore.utils.TextUtils;
import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import com.infiniteplugins.infinitedispensers.settings.Settings;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Utils {

	public static ItemStack getGlass() {
		return getItem("menus.glass-1");
	}

	public static ItemStack getBackgroundGlass(boolean type) {
		if (type)
			return getItem("menus.glass-2");
		else
			return getItem("menus.glass-3");
	}
	
	public static void playParticles(String name, int amount, Location... locations) {
		playParticles(name, amount, 0, 0, 0, locations);
	}
	
	public static void playParticles(String name, int amount, double offsetX, double offsetY, double offsetZ, Location... locations) {
		if (!Utils.classExists("org.bukkit.Particle")) {
			Effect effect;
			try {
				effect = Effect.valueOf(name);
			} catch (Exception e) {
				if (!Settings.WRONG_ENUM.getBoolean() == false)
					InfiniteDispensers.getInstance().getLogger().info("The particle " + InfiniteDispensers.getInstance().getConfiguration("upgrading").getString("upgrading.particle", "SPELL_WITCH") + " could not be found. Make sure you're using the particle naming for your server version. Using default value...");
				// Fallback for 1.8
				try {
					effect = Effect.valueOf("LAVA_POP");
				} catch (Exception e1) {
					effect = Effect.CLICK1;
				}
			}
			for (Location location : locations) {
				for (int i = 0; i < amount; i++) {
					location.getWorld().playEffect(location.clone().add(offsetX, offsetY, offsetZ), effect, 0);
				}
			}
		} else {
			Particle particle;
			try {
				particle = Particle.valueOf(name);
			} catch (Exception e) {
				if (!Settings.WRONG_ENUM.getBoolean() == false)
					InfiniteDispensers.getInstance().getLogger().info("The particle " + InfiniteDispensers.getInstance().getConfiguration("upgrading").getString("upgrading.particle", "SPELL_WITCH") + " could not be found. Make sure you're using the particle naming for your server version. Using default value...");
				// Generic 1.9+ fallback
				try {
					particle = Particle.valueOf("FLAME");
				} catch (Exception e1) {
					particle = Particle.CLOUD;
				}
			}
			for (Location location : locations) {
				location.getWorld().spawnParticle(particle, location, amount, offsetX, offsetY, offsetZ);
			}
		}
	}
	
	/**
	 * Tests whether a given class exists in the classpath.
	 * 
	 * @author Skript team.
	 * @param className The {@link Class#getCanonicalName() canonical name} of the class
	 * @return Whether the given class exists.
	 */
	public static boolean classExists(String className) {
		try {
			Class.forName(className);
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}

	/**
	 * Tests whether a method exists in the given class.
	 * 
	 * @author Skript team.
	 * @param c The class
	 * @param methodName The name of the method
	 * @param parameterTypes The parameter types of the method
	 * @return Whether the given method exists.
	 */
	public static boolean methodExists(Class<?> c, String methodName, Class<?>... parameterTypes) {
		try {
			c.getDeclaredMethod(methodName, parameterTypes);
			return true;
		} catch (NoSuchMethodException | SecurityException e) {
			return false;
		}
	}
	
	/**
	 * Tests whether a method exists in the given class, and whether the return type matches the expected one.
	 * <p>
	 * Note that this method doesn't work properly if multiple methods with the same name and parameters exist but have different return types.
	 * 
	 * @author Skript team.
	 * @param c The class
	 * @param methodName The name of the method
	 * @param parameterTypes The parameter types of the method
	 * @param returnType The expected return type
	 * @return Whether the given method exists.
	 */
	public static boolean methodExists(Class<?> c, String methodName, Class<?>[] parameterTypes, Class<?> returnType) {
		try {
			Method m = c.getDeclaredMethod(methodName, parameterTypes);
			return m.getReturnType() == returnType;
		} catch (NoSuchMethodException | SecurityException e) {
			return false;
		}
	}

	public static String formatName(int level, boolean full) {
		String name = InfiniteDispensers.getInstance().getLocale().getMessage("name-format")
				.processPlaceholder("level", level + "").getMessage();
		String info = "";
		if (full) {
			info += Formatting.convertToInvisibleString(level + ":");
		}
		return info + TextUtils.formatText(name);
	}
	
	public static Material materialAttempt(String attempt, String fallback) {
		Material material = null;
		try {
			material = Material.valueOf(attempt);
		} catch (Exception e) {
			try {
				material = Material.valueOf(fallback);
			} catch (Exception e1) {}
		}
		if (material == null)
			material = Material.CHEST;
		return material;
	}
	
	public static List<Block> getNearbyBlocks(Location location, int radiusx, int radiusz, int radiusy) {
		List<Block> blocks = new ArrayList<Block>();
		for(int x = location.getBlockX() - radiusx; x <= location.getBlockX() + radiusx; x++) {
			for(int y = location.getBlockY() - radiusy; y <= location.getBlockY() + radiusy; y++) {
				for(int z = location.getBlockZ() - radiusz; z <= location.getBlockZ() + radiusz; z++) {
					blocks.add(location.getWorld().getBlockAt(x, y, z));
				}
			}
		}
		return blocks;
	}
	
	public static ItemStack getItem(String node) {
		return getItem(node, "STAINED_GLASS_PANE");
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack getItem(String node, String fallback) {
		Config configuration = InfiniteDispensers.getInstance().getMenuConfig();
		Material material = null;
		ItemStack item = new ItemStack(Material.GLASS, 1, (short) 1);
		try {
			material = Material.valueOf(configuration.getString(node + ".material", "GRAY_STAINED_GLASS_PANE"));
		} catch (Exception e) {
			if (!Settings.WRONG_ENUM.getBoolean() == false)
				InfiniteDispensers.getInstance().getLogger().log(Level.SEVERE, "The material " + configuration.getString(node + ".material", "STAINED_GLASS_PANE") + " could not be found. Make sure you're using the material naming for your server version, Try " + fallback + ". Using default value...  (Configuration: " + node + ".material)");
			// Find a default value for their version, because the user's input is not working.
			material = materialAttempt(fallback, "GRAY_STAINED_GLASS_PANE");
		}
		if (material != null) {
			if (material.name().startsWith("STAINED_GLASS") || material.name().contains("WOOL")) {
				int type;
				if (configuration.getBoolean(node + ".rainbow", false)) {
					type = 1 + (int) (Math.random() * 6);
				} else {
					type = configuration.getInt(node + ".color", 7);
				}
				item = new ItemStack(material, 1, (short) type);
			} else {
				item = new ItemStack(material, 1);
			}
		}
		String name = TextUtils.formatText(configuration.getString(node + ".name", "&1"));
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}
	
	public static InfiniteSound getSound(FileConfiguration configuration, String node) {
		boolean enabled = configuration.getBoolean(node + ".enabled", true);
		Sound sound = Sound.BLOCK_CHEST_OPEN;
		float pitch, volume;
		try {
			sound = Sound.valueOf(configuration.getString(node + ".sound", "BLOCK_NOTE_BLOCK_PLING"));
		} catch (Exception e) {
			if (!Settings.WRONG_ENUM.getBoolean() == false)
				InfiniteDispensers.getInstance().getLogger().log(Level.SEVERE, "The sound effect " + configuration.getString(node + ".sound", "null") + " was not found, make sure you're using the correct sounds for your version. Using default value... (Configuration: " + node + ".sound)");
			try {
				// Find a default value for their version, because the user's input is not working.
				sound = Sound.valueOf("BLOCK_NOTE_BLOCK_PLING");
				sound = Sound.valueOf("CLICK");
			} catch (Exception empty) {}
		}
		pitch = (float) configuration.getDouble(node + ".pitch", 1.0D);
		volume = (float) configuration.getDouble(node + ".volume", 1.0D);
		return new InfiniteSound(sound, pitch, volume, enabled);
	}

}
