package com.infiniteplugins.infinitedispensers.utils;

import com.infiniteplugins.infinitedispensers.InfiniteDispensers;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Formatting {
	
	public static String messagesPrefixed(FileConfiguration configuration, String... nodes) {
		FileConfiguration messages = InfiniteDispensers.getInstance().getConfiguration("messages");
		String complete = messages.getString("messages.prefix", "&8[&6EpicDispensers&8] &r");
		return Formatting.color(complete + messages(configuration, Arrays.copyOfRange(nodes, 0, nodes.length)));
	}
	
	public static String messages(FileConfiguration configuration, String... nodes) {
		String complete = "";
		List<String> list = Arrays.asList(nodes);
		Collections.reverse(list);
		for (String node : list.toArray(new String[list.size()])) {
			complete = configuration.getString(node, "Not set") + " " + complete;
		}
		return Formatting.color(complete);
	}
	
	public static String economy(double input) {
		FileConfiguration settings = InfiniteDispensers.getInstance().getConfig();
		DecimalFormat formatter = new DecimalFormat(settings.getString("economy.economy-format", "#,###.00"));
		return formatter.format(input);
	}

	
	public static String color(String input) {
		return ChatColor.translateAlternateColorCodes('&', input);
	}
	
	public static String stripColor(String input) {
		return ChatColor.stripColor(input);
	}
	
	public static String convertToInvisibleString(String s) {
		if (s == null || s.equals(""))
			return "";
		StringBuilder hidden = new StringBuilder();
		for (char c : s.toCharArray()) hidden.append(ChatColor.COLOR_CHAR + "").append(c);
		return hidden.toString();
	}
	
}
