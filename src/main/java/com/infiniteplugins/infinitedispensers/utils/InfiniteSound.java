package com.infiniteplugins.infinitedispensers.utils;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class InfiniteSound {
	
	private final float pitch, volume;
	private final boolean enabled;
	private final Sound sound;
	private final int delay;
	
	public InfiniteSound(Sound sound, float pitch, float volume, boolean enabled) {
		this.enabled = enabled;
		this.volume = volume;
		this.sound = sound;
		this.pitch = pitch;
		this.delay = 0;
	}
	
	public int getDelay() {
		return delay;
	}
	
	public Sound getSound() {
		return sound;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public float getVolume() {
		return volume;
	}
	
	public void playTo(Player... players) {
		if (enabled) {
			for (Player player : players) {
				player.playSound(player.getLocation(), sound, volume, pitch);
			}
		}
	}
	
	public void playAt(Location... locations) {
		if (enabled) {
			for (Location location : locations) {
				location.getWorld().playSound(location, sound, volume, pitch);
			}
		}
	}
	
}